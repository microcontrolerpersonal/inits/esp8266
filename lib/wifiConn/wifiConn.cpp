#include "wifiConn.h"

#if SETUP_MDNS == 1
void setupMDNS()
{
    if (!MDNS.begin(MDNS_NAME))
    {
        logSf(LEVEL::error, "Error setting up MDNS responder!");
    }
    else
    {
        logSf(LEVEL::info, "mDNS responder started (%s.local)", MDNS_NAME);
        blink(2);
    }
}
#endif

void listNetworks()
{
    int netNum = WiFi.scanNetworks();
    char ssid[33];
    Serial.printf("Found %i APs\n", netNum);
    Serial.printf("%-32s|%s\n", "SSID", "RSSI");
    for (int i = 0; i < netNum; i++)
    {
        WiFi.SSID(i).toCharArray(ssid, 33);
        Serial.printf("%-32s|%i\n", ssid, WiFi.RSSI(i));
    }
    Serial.println();
}

void setupStaticIP(NetworkInfo networkInfo)
{
    if (networkInfo.mask[3] != 0)
    {
        // weird small mask, maybe i'll do sth in these cases later, doubt it tho.
        Serial.println("Mask too restrictive");
    }
    else
    {
        logS(LEVEL::debug, "Trying to change ip to: ");
        networkInfo.ip[3] = IP_LAST_OCT;
        Serial.println(networkInfo.ip);

        for (int loops = 10; loops > 0; loops--)
        {
            if (WiFi.config(networkInfo.ip, networkInfo.gate, networkInfo.mask))
            {
                logS(LEVEL::info, "IP address: ");
                networkInfo.ip = WiFi.localIP();
                logS(LEVEL::info, networkInfo.ip, true);
                blink(1);
                break;
            }
            else
            {
                logSLn(LEVEL::info, loops);
                delay(1000);
            }
        }
    }
    logSLn(LEVEL::info, true);
}

NetworkInfo basicConnect()
{
    NetworkInfo networkInfo = NetworkInfo();
    WiFi.begin(YOUR_SSID, YOUR_PSK);
    logSLn(LEVEL::info, "Connecting to WiFI");
    for (int loops = 10; loops > 0; loops--)
    {
        digitalWrite(LED_BUILTIN, loops % 2);
        if (WiFi.status() == WL_CONNECTED)
        {
            logSLn(LEVEL::info, "WiFi connected ");
            logS(LEVEL::info, "SSID: ");
            logSLn(LEVEL::info, WiFi.SSID(), true);
            logS(LEVEL::info, "IP address: ");
            networkInfo.ip = WiFi.localIP();
            logSLn(LEVEL::info, networkInfo.ip, true);
            logS(LEVEL::info, "Hostname: ");
            logSLn(LEVEL::info, WiFi.getHostname(), true);
            logS(LEVEL::info, "Mask: ");
            networkInfo.mask = WiFi.subnetMask();
            logSLn(LEVEL::info, networkInfo.mask, true);
            logS(LEVEL::info, "Gate: ");
            networkInfo.gate = WiFi.gatewayIP();
            logSLn(LEVEL::info, networkInfo.gate, true);
            break;
        }
        else
        {
            logSLn(LEVEL::info, loops);
            delay(1000);
        }
    }
    if (WiFi.status() != WL_CONNECTED)
    {
        logSLn(LEVEL::info, "WiFi connect failed");
        delay(100);
        ESP.restart();
    }
    else
    {
        blink(5);
    }
    return networkInfo;
}

void setupWiFi()
{
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
#if LIST_NETWORKS == 1
    listNetworks();
#endif
    NetworkInfo networkInfo = basicConnect();
#if SETUP_STATIC_IP == 1
    setupStaticIP(networkInfo);
#endif
#if SETUP_MDNS == 1
    setupMDNS();
#endif
    logSLn(LEVEL::info, "Wifi Ready!");
}

void loopWiFi(){
#if SETUP_MDNS == 1
    MDNS.update();
#endif
}