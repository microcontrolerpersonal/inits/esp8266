#pragma once
#include "defines.h"
#include "betterSerial.h"
#include "blinkCodes.h"
#include <ESP8266WiFi.h>
#if SETUP_MDNS == 1
#include <ESP8266mDNS.h>
#endif
#include "wifiCreds.h"

inline WiFiServer server(23);
inline WiFiClient serverClient;

struct NetworkInfo{
    IPAddress mask = IPAddress(0,0,0,0);
    IPAddress ip = IPAddress(0,0,0,0);
    IPAddress gate = IPAddress(0,0,0,0);
};

void setupMDNS();
void listNetworks();
void setupStaticIP(NetworkInfo networkInfo);
NetworkInfo basicConnect();
void setupWiFi();
void loopWiFi();