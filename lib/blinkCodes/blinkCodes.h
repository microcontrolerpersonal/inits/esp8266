#pragma once
#include <Arduino.h>

void setupBlink(int blinkPin = LED_BUILTIN, bool active = LOW);
void blink();
void blink(int num);