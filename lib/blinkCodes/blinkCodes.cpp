#include "blinkCodes.h"

int _blinkPin;
bool _active;

void setupBlink(int blinkPin, bool active){
    #if BLINK_CODES == 1
    _blinkPin = blinkPin;
    _active = active;
    pinMode(_blinkPin, OUTPUT);
    #endif
}

void blink(){
    #if BLINK_CODES == 1
    digitalWrite(_blinkPin, _active);
    delay(100);
    digitalWrite(_blinkPin, !_active);
    delay(100);
    #endif
}

void blink(int num){
    #if BLINK_CODES == 1
    for (int i = 0; i < num; i++){
        blink();
    }
    delay(200);
    #endif
}