#include "betterSerial.h"

HardwareSerial *bSerial;
String _LEVEL_NAMES[] = {"debug", "info", "warn", "error", "fatal", ""};

void setupSerial(HardwareSerial *chosenSerial)
{
    bSerial = chosenSerial;
    bSerial->begin(BAUD_RATE);
}

void _logPrefix(LEVEL lvl, bool ignorePrefix)
{
    if (ignorePrefix)
    {
        return;
    }
    if (lvl == LEVEL::none)
    {
        return;
    }
    bSerial->write("[");
    bSerial->print(_LEVEL_NAMES[lvl]);
    bSerial->write("] ");
}

template <class T>
void _logS(LEVEL lvl, T var, bool ignorePrefix)
{
#if MIN_LEVEL >= lvl
    _logPrefix(lvl, ignorePrefix);
    bSerial->print(var);
#endif
}

template <class T>
void _logS(LEVEL lvl, T var, int base, bool ignorePrefix)
{
#if MIN_LEVEL >= lvl
    _logPrefix(lvl, ignorePrefix);
    bSerial->print(var, base);
#endif
}

template <class T>
void _logSLn(LEVEL lvl, T var, bool ignorePrefix)
{
#if MIN_LEVEL >= lvl
    _logPrefix(lvl, ignorePrefix);
    bSerial->println(var);
#endif
}

template <class T>
void _logSLn(LEVEL lvl, T var, int base, bool ignorePrefix)
{
#if MIN_LEVEL >= lvl
    _logPrefix(lvl, ignorePrefix);
    bSerial->println(var, base);
#endif
}

void _logS(LEVEL lvl, const Printable &var, bool ignorePrefix)
{
#if MIN_LEVEL >= lvl
    _logPrefix(lvl, ignorePrefix);
    bSerial->print(var);
#endif
}

void _logSLn(LEVEL lvl, const Printable &var, bool ignorePrefix)
{
#if MIN_LEVEL >= lvl
    _logPrefix(lvl, ignorePrefix);
    bSerial->println(var);
#endif
}

void logSf(LEVEL lvl, const char *format, ...)
{
#if MIN_LEVEL >= lvl
    _logPrefix(lvl, false);
    bSerial->println(format);
    va_list arg; // original printf begin
    va_start(arg, format);
    char temp[64];
    char *buffer = temp;
    size_t len = vsnprintf(temp, sizeof(temp), format, arg);
    va_end(arg);
    if (len > sizeof(temp) - 1)
    {
#ifndef __INTELLISENSE__ // ugly hack to get rid of an error
        buffer = new (std::nothrow) char[len + 1];
#endif
        if (!buffer)
        {
            return;
        }
        va_start(arg, format);
        vsnprintf(buffer, len + 1, format, arg);
        va_end(arg);
    }
    if (buffer != temp)
    {
        delete[] buffer;
    } // original printf end
#endif
}

void logS(LEVEL lvl, const String &s, bool ignorePrefix)
{
    _logS(lvl, s, ignorePrefix);
}

void logS(LEVEL lvl, const char str[], bool ignorePrefix)
{
    _logS(lvl, str, ignorePrefix);
}

void logS(LEVEL lvl, char c, bool ignorePrefix)
{
    _logS(lvl, c, ignorePrefix);
}

void logS(LEVEL lvl, unsigned char c, int base, bool ignorePrefix)
{
    _logS(lvl, c, base, ignorePrefix);
}

void logS(LEVEL lvl, int c, int base, bool ignorePrefix)
{
    _logS(lvl, c, base, ignorePrefix);
}

void logS(LEVEL lvl, unsigned int c, int base, bool ignorePrefix)
{
    _logS(lvl, c, base, ignorePrefix);
}

void logS(LEVEL lvl, long c, int base, bool ignorePrefix)
{
    _logS(lvl, c, base, ignorePrefix);
}

void logS(LEVEL lvl, long long c, int base, bool ignorePrefix)
{
    _logS(lvl, c, base, ignorePrefix);
}

void logS(LEVEL lvl, unsigned long c, int base, bool ignorePrefix)
{
    _logS(lvl, c, base, ignorePrefix);
}

void logS(LEVEL lvl, unsigned long long c, int base, bool ignorePrefix)
{
    _logS(lvl, c, base, ignorePrefix);
}

void logS(LEVEL lvl, const Printable &x, bool ignorePrefix)
{
    _logS(lvl, x, ignorePrefix);
}

void logSLn(LEVEL lvl, bool ignorePrefix)
{
    _logS(lvl, "\r\n", ignorePrefix);
}

void logSLn(LEVEL lvl, const String &s, bool ignorePrefix)
{
    _logSLn(lvl, s, ignorePrefix);
}

void logSLn(LEVEL lvl, const char str[], bool ignorePrefix)
{
    _logSLn(lvl, str, ignorePrefix);
}

void logSLn(LEVEL lvl, char c, bool ignorePrefix)
{
    _logSLn(lvl, c, ignorePrefix);
}

void logSLn(LEVEL lvl, unsigned char c, int base, bool ignorePrefix)
{
    _logSLn(lvl, c, base, ignorePrefix);
}

void logSLn(LEVEL lvl, int c, int base, bool ignorePrefix)
{
    _logSLn(lvl, c, base, ignorePrefix);
}

void logSLn(LEVEL lvl, unsigned int c, int base, bool ignorePrefix)
{
    _logSLn(lvl, c, base, ignorePrefix);
}

void logSLn(LEVEL lvl, long c, int base, bool ignorePrefix)
{
    _logSLn(lvl, c, base, ignorePrefix);
}

void logSLn(LEVEL lvl, long long c, int base, bool ignorePrefix)
{
    _logSLn(lvl, c, base, ignorePrefix);
}

void logSLn(LEVEL lvl, unsigned long c, int base, bool ignorePrefix)
{
    _logSLn(lvl, c, base, ignorePrefix);
}

void logSLn(LEVEL lvl, unsigned long long c, int base, bool ignorePrefix)
{
    _logSLn(lvl, c, base, ignorePrefix);
}

void logSLn(LEVEL lvl, const Printable &x, bool ignorePrefix)
{
    _logSLn(lvl, x, ignorePrefix);
}