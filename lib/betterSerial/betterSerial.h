#pragma once
#include <Arduino.h>
#include "defines.h"

enum LEVEL{
    debug = 0,
    info = 1,
    warn = 2,
    error = 3,
    fatal = 4,
    none = 5
};

void setupSerial(HardwareSerial* chosenSerial = &Serial);

void logSf(LEVEL lvl, const char *format, ...);

void logS(LEVEL lvl, const String &s, bool ignorePrefix = false);
void logS(LEVEL lvl, const char str[], bool ignorePrefix = false);
void logS(LEVEL lvl, char c, bool ignorePrefix = false);
void logS(LEVEL lvl, unsigned char c, int base = 10, bool ignorePrefix = false);
void logS(LEVEL lvl, int c, int base = 10, bool ignorePrefix = false);
void logS(LEVEL lvl, unsigned int c, int base = 10, bool ignorePrefix = false);
void logS(LEVEL lvl, long c, int base = 10, bool ignorePrefix = false);
void logS(LEVEL lvl, long long c, int base = 10, bool ignorePrefix = false);
void logS(LEVEL lvl, unsigned long c, int base = 10, bool ignorePrefix = false);
void logS(LEVEL lvl, unsigned long long c, int base = 10, bool ignorePrefix = false);
void logS(LEVEL lvl, const Printable& x, bool ignorePrefix = false);
void logSLn(LEVEL lvl, bool ignorePrefix = false);
void logSLn(LEVEL lvl, const String &s, bool ignorePrefix = false);
void logSLn(LEVEL lvl, const char str[], bool ignorePrefix = false);
void logSLn(LEVEL lvl, char c, bool ignorePrefix = false);
void logSLn(LEVEL lvl, unsigned char c, int base = 10, bool ignorePrefix = false);
void logSLn(LEVEL lvl, int c, int base = 10, bool ignorePrefix = false);
void logSLn(LEVEL lvl, unsigned int c, int base = 10, bool ignorePrefix = false);
void logSLn(LEVEL lvl, long c, int base = 10, bool ignorePrefix = false);
void logSLn(LEVEL lvl, long long c, int base = 10, bool ignorePrefix = false);
void logSLn(LEVEL lvl, unsigned long c, int base = 10, bool ignorePrefix = false);
void logSLn(LEVEL lvl, unsigned long long c, int base = 10, bool ignorePrefix = false);
void logSLn(LEVEL lvl, const Printable& x, bool ignorePrefix = false);