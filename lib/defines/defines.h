#pragma once
#define DEBUG 1
#define INFO 1
#define BLINK_CODES 1
// ---- WIFI ----
#define LIST_NETWORKS 1
#define SETUP_MDNS 1
#define MDNS_NAME "espempty"
#define SETUP_STATIC_IP 1
#define IP_LAST_OCT 99
// ---- Serial ----
#define BAUD_RATE 115200
#define MIN_LEVEL 2 //debug = 1,info = 2,warn = 3,error = 4,fatal = 5,none = 6