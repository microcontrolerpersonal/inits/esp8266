#include <Arduino.h>
#include "betterSerial.h"
#include "wifiConn.h"
#include "ota.h"

void setup()
{
    setupSerial(&Serial);
    setupBlink();
    setupWiFi();
    setupOTA();
}

void loop()
{
    loopWiFi();
    loopOTA();
}